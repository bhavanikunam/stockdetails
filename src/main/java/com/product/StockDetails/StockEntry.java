package com.product.StockDetails;

public class StockEntry {
	private String requestTimestamp;

	private Stock stock;

	private String productid;

	public String getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(String requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {

		this.stock = stock;

	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	@Override
	public String toString() {
		return "[requestTimestamp = " + requestTimestamp + ", stock = " + stock + ", productid = " + productid + "]";
	}
}