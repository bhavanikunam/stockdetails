package com.product.StockDetails;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("product")
public class StockResource {

	StockRepository stk = new StockRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Stock> getStockDetails() {
		return stk.getStockAllDetails();
	}

	@GET
	@Path("stock")
	@Produces(MediaType.APPLICATION_JSON)
	public StockEntry getProductDetails(@QueryParam("productId") String productid) {
		return stk.getProduct(productid);
	}

	@POST
	@Path("updateStock")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("text/html")
	public Response updateStock(Stock a1) {
		String output;
		if (a1.getId().equals(stk.getProductId(a1.getId()).getId()))

		{
			output = "outdated stock, because a newer stock was processed first";
			return Response.status(204).entity(output).build();
		} else {
			stk.createStockEntry(a1);
			output = "everything went well and the new entry for stock was accepted";
			return Response.status(201).entity(output).build();
		}
	}

	@GET
	@Path("statistics")
	@Produces(MediaType.APPLICATION_JSON)
	public StockStatistics getStatistics(@QueryParam("time") List<String> range) {
		return stk.getStockStatistics();
	}

}
