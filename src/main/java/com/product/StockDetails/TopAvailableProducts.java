package com.product.StockDetails;

public class TopAvailableProducts {
	private String timestamp;

	private String id;

	private String quantity;

	private String productId;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "[timestamp = " + timestamp + ", id = " + id + ", productId = " + productId + ",quantity = " + quantity
				+ "]";
	}
}
