package com.product.StockDetails;

public class TopSellingProducts {
	private String itemsSold;

	private String productId;

	public String getItemsSold() {
		return itemsSold;
	}

	public void setItemsSold(String itemsSold) {
		this.itemsSold = itemsSold;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "[itemsSold = " + itemsSold + ", productId = " + productId + "]";
	}
}