package com.product.StockDetails;

public class StockStatistics {
	private String requestTimestamp;

	private String range;

	private TopSellingProducts[] topSellingProducts;

	private TopAvailableProducts[] topAvailableProducts;

	public String getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(String requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public TopSellingProducts[] getTopSellingProducts() {
		return topSellingProducts;
	}

	public void setTopSellingProducts(TopSellingProducts[] topSellingProducts) {
		this.topSellingProducts = topSellingProducts;
	}

	public TopAvailableProducts[] getTopAvailableProducts() {
		return topAvailableProducts;
	}

	public void setTopAvailableProducts(TopAvailableProducts[] topAvailableProducts) {
		this.topAvailableProducts = topAvailableProducts;
	}

	@Override
	public String toString() {

		return " [requestTimestamp = " + requestTimestamp + ", range = " + range + ", topSellingProducts = "
				+ topSellingProducts + ", topAvailableProducts = " + topAvailableProducts + "]";
	}
}
