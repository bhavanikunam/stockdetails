package com.product.StockDetails;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class StockRepository {

	Connection con = null;

	public StockRepository() {
		String url = "jdbc:mysql://localhost:3306/stock";
		String username = "root";
		String password = "1234";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Stock getProductId(String id) {
		Stock a = new Stock();
		String sql = "select * from stock_details where id=" + id;
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {

				a.setId(rs.getString(1));
				a.setTimestamp(rs.getTimestamp(2));
				a.setProductid(rs.getString(3));
				a.setQuantity(rs.getInt(4));
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return a;
	}

	public void createStockEntry(Stock a1) {
		String sql = "insert into stock_details values(?,?,?,?)";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, a1.getId());
			st.setTimestamp(2, a1.getTimestamp());
			st.setString(3, a1.getProductid());
			st.setInt(4, a1.getQuantity());
			st.executeUpdate();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public StockEntry getProduct(String productid) {
		Stock a = new Stock();
		StockEntry b = new StockEntry();
		String timeStamp = Instant.now().toString();

		String sql = "select * from stock_on_hand where productid=\'" + productid + "\'";
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {

				a.setId(rs.getString(1));
				a.setTimestamp(rs.getTimestamp(2));
				b.setProductid(rs.getString(3));
				a.setQuantity(rs.getInt(4));

			}

			b.setStock(a);
			b.setRequestTimestamp(timeStamp);

		} catch (Exception e) {
			System.out.println(e);
		}
		return b;
	}

	public List<Stock> getStockAllDetails() {
		List<Stock> stock = new ArrayList<>();
		String sql = "select * from stock_details";
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Stock a = new Stock();
				a.setId(rs.getString(1));
				a.setTimestamp(rs.getTimestamp(2));
				a.setProductid(rs.getString(3));
				a.setQuantity(rs.getInt(4));
				stock.add(a);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return stock;
	}

	public StockStatistics getStockStatistics() {
		String TODAY = "today-lastMonth";
		List<TopAvailableProducts> list = new ArrayList<>();
		List<TopSellingProducts> list2 = new ArrayList<>();

		String timeStamp = Instant.now().toString();
		StockStatistics s = new StockStatistics();
		String sql = " select soh.id,soh.timestamp,stk.productid,stk.total from (select sum(quantity) total,productid from stock_details where MONTH(timestamp)>= MONTH(CURRENT_DATE - INTERVAL 1 MONTH) group by productid order by  total desc LIMIT 3) stk,stock_on_hand soh where stk.productid=soh.productid";
		String sql2 = "select abs(sum(quantity)) soh,productid from stock_details where MONTH(timestamp)>= MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and quantity like '-%' group by productid order by  soh desc LIMIT 3";
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				TopAvailableProducts t = new TopAvailableProducts();
				t.setId(rs.getString(1));
				t.setTimestamp(rs.getString(2));
				t.setProductId(rs.getString(3));
				t.setQuantity(rs.getString(4));
				list.add(t);
			}

			ResultSet rs2 = st.executeQuery(sql2);
			while (rs2.next()) {
				TopSellingProducts sold = new TopSellingProducts();
				sold.setItemsSold(rs2.getString(1));
				sold.setProductId(rs2.getString(2));
				list2.add(sold);
			}
			s.setRequestTimestamp(timeStamp);
			s.setRange(TODAY);

			TopAvailableProducts[] top = list.toArray(new TopAvailableProducts[list.size()]);

			s.setTopAvailableProducts(top);
			TopSellingProducts[] sell = list2.toArray(new TopSellingProducts[list2.size()]);
			s.setTopSellingProducts(sell);

		} catch (Exception e) {
			System.out.println(e);
		}
		return s;

	}
}
