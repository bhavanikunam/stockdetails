-- MySQL dump 10.13  Distrib 5.7.22, for Win64 (x86_64)
--
-- Host: localhost    Database: stock
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stock_details`
--

DROP TABLE IF EXISTS `stock_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_details` (
  `ID` varchar(255) NOT NULL,
  `timestamp` datetime(3) NOT NULL,
  `productid` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_details`
--

LOCK TABLES `stock_details` WRITE;
/*!40000 ALTER TABLE `stock_details` DISABLE KEYS */;
INSERT INTO `stock_details` VALUES ('1','2017-07-15 20:54:01.754','fruit-111',100),('10','2017-07-14 20:54:01.754','fruit-333',-10),('11','2018-07-14 20:54:01.754','fruit-333',5),('111','2018-07-16 02:24:01.754','vegetable-123',10),('12','2018-07-14 20:54:01.754','fruit-222',20),('13','2018-07-14 12:54:01.754','fruit-222',-10),('14','2018-07-14 20:54:01.754','fruit-111',10),('15','2018-07-14 20:54:01.754','fruit-111',-10),('16','2018-07-14 20:54:01.754','vegetable-123',20),('17','2018-07-14 20:54:01.754','vegetable-123',50),('18','2018-07-14 20:54:01.754','vegetable-123',-20),('19','2018-07-14 20:54:01.754','vegetable-123',-10),('2','2018-07-02 02:24:01.754','fruit-111',10),('3','2018-07-02 02:24:01.754','vegetable-123',50),('4','2018-07-03 02:24:01.754','vegetable-123',70),('5','2018-07-04 02:24:01.754','vegetable-123',-20),('555','2017-07-16 02:24:01.754','fruit-111',-10),('666','2017-07-16 02:24:01.754','fruit-333',20);
/*!40000 ALTER TABLE `stock_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp850 */ ;
/*!50003 SET character_set_results = cp850 */ ;
/*!50003 SET collation_connection  = cp850_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER trig_stock_on_hand
AFTER INSERT ON stock.stock_details  FOR EACH ROW
begin

       IF exists(select 1 from stock_on_hand
       WHERE productid= NEW.productid)
       THEN
           UPDATE stock_on_hand
           SET 
   id=NEW.id,
   timestamp=NEW.timestamp,
   quantity=quantity+NEW.quantity
           WHERE productid= NEW.productid;
ELSE
             insert into stock_on_hand
(id,timestamp,productid,quantity)
values
(NEW.id,NEW.timestamp,NEW.productid,NEW.quantity);
        END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `stock_on_hand`
--

DROP TABLE IF EXISTS `stock_on_hand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_on_hand` (
  `ID` varchar(255) DEFAULT NULL,
  `timestamp` datetime(3) NOT NULL,
  `productid` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_on_hand`
--

LOCK TABLES `stock_on_hand` WRITE;
/*!40000 ALTER TABLE `stock_on_hand` DISABLE KEYS */;
INSERT INTO `stock_on_hand` VALUES ('555','2017-07-16 02:24:01.754','fruit-111',100),('13','2018-07-14 12:54:01.754','fruit-222',10),('666','2017-07-16 02:24:01.754','fruit-333',15),('111','2018-07-16 02:24:01.754','vegetable-123',150);
/*!40000 ALTER TABLE `stock_on_hand` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-16  0:15:37
