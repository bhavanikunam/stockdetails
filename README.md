Project : StockDetails

Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

Prerequisites:

jdk 1.8.0_171
Tomcat Server 8.5
MySQL server 5.7.22-log
Maven 2.5.1
Jersey 2.22.2
Eclipse Java EE IDE : Photon Release (4.8.0)
Postman

Dependencies
mysql-connector-java of version 5.1.38
jersey-media-moxy

Building
1. clone the the specified bitbucket repository "https://bhavanikunam@bitbucket.org/bhavanikunam/StockDetails.git"
2. Create a database "stock" in MySQL and import the file stock.sql
3. Open the StockDetails project after repository download in Eclipse IDE.
4. Right click on project created and click on properties->Target Runtimes select 'Apache Tomcat v8.5'. Click Apply and close to finish the project setup
5. Check dependencies in pom.xml
6. Change the url,username,password in <ProjectPATH>\StockDetails\src\main\java\com\product\StockDetails\StockRepository.java file for MySql DB connection in StockRepository() as per localhost db
7. Right click on project StockDetails and select 'Run As' and select 'Run on server' and finish to run the project on server.
8. Use Postman to Query the API URL's as per the request methods and get the responses. Results are displayed  as shown in "Results" file


Results:
Task1: 
Place the following URL and choose POST in postman and place the JSON object(sample format as follows). If the ID is new entry, it will be inserted in DB and returns status code 201 . If ID is already existing then it will not insert given details  and return status code 204.
http://localhost:8085/StockDetails/webapi/product/updatestock
JSON:
 {
        "id": "777",
        "productid": "fruit-333",
        "quantity": 20,
        "timestamp": "2017-07-15T20:54:01.754Z"
    }
	
Following URL will list all the entries of stock details in DB
http://localhost:8085/StockDetails/webapi/product	
	

Task2: 
Place the following URL and choose GET in postman to get current stock available for productId in JSON format. ProductId can be changed as per expecting product details
http://localhost:8085/StockDetails/webapi/product/stock/productId=vegetable-123

Task3:
Place the following URL and choose GET in postman to get the statistics from from last month till current date. ID and timestamp returned for top available products will be recent updated details for same product.
http://localhost:8085/StockDetails/webapi/product/statistics?time=today










